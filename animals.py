elephants = [
    {'id': 1, 'name': 'karen', 'age': 44},
    {'id': 2, 'name': 'bob', 'age': 2},
    {'id': 3, 'name': 'michael', 'age': 2},
]

giraffes = {'giraffes': [
    {'id': 4, 'name': 'monica', 'age': 19},
    {'id': 2, 'name': 'reeta', 'age': 31},
    {'id': 3, 'name': 'sandra', 'age': 15},
]}

monkeys = {'monkeys': [
    {'id': 7, 'name': 'donny', 'age': 22},
    {'id': 2, 'name': 'mikey', 'age': 23},
    {'id': 3, 'name': 'raph', 'age': 21},
]}