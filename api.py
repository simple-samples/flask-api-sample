import flask
from animals import *
from flask import request, jsonify

app = flask.Flask(__name__)
app.config["DEBUG"] = True


@app.route('/', methods=['GET'])
def home():
    return '''<h1> ZOO ANIMALS </h1>'''


@app.route('/api/v1/elephants/all', methods=['GET'])
def elephants_all():
    return jsonify(elephants)


@app.route('/api/v1/elephants', methods=['GET'])
def elephants_id():
    # Check if an ID was provided as part of the URL.
    # If ID is provided, assign it to a variable.
    # If no ID is provided, display an error in the browser
    if 'id' in request.args:
        id_request = int(request.args['id'])
    else:
        return "Error: No id field provided. Please specify an id."
    results = []
    for elephant in elephants:
        if elephant['id'] == id_request:
            results.append(elephant)
            print("match found")
    return jsonify(results)


@app.route('/api/v1/giraffes/all', methods=['GET'])
def elephants_all():
    return jsonify(giraffes)


@app.route('/api/v1/giraffes', methods=['GET'])
def elephants_id():
    # Check if an ID was provided as part of the URL.
    # If ID is provided, assign it to a variable.
    # If no ID is provided, display an error in the browser
    if 'id' in request.args:
        id_request = int(request.args['id'])
    else:
        return "Error: No id field provided. Please specify an id."
    results = []
    for giraffe in giraffes:
        if giraffe['id'] == id_request:
            results.append(giraffes)
            print("match found")
    return jsonify(results)


@app.route('/api/v1/monkeys/all', methods=['GET'])
def elephants_all():
    return jsonify(giraffes)


@app.route('/api/v1/monkeys', methods=['GET'])
def elephants_id():
    # Check if an ID was provided as part of the URL.
    # If ID is provided, assign it to a variable.
    # If no ID is provided, display an error in the browser
    if 'id' in request.args:
        id_request = int(request.args['id'])
    else:
        return "Error: No id field provided. Please specify an id."
    results = []
    for monkey in monkeys:
        if monkey['id'] == id_request:
            results.append(monkeys)
            print("match found")
    return jsonify(results)


if __name__ == '__main__':
    app.run()
